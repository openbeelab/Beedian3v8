��    �     t  9  �      �#  w   �#  '   )$  
   Q$  L   \$     �$     �$     �$     �$  =   �$     %  6   $%     [%  	   d%     n%  .   �%  .   �%  %   �%     &     "&     ?&     Y&      o&  "   �&  &   �&  -   �&     '     '      +'  <   L'     �'     �'     �'     �'     �'     (      (  -   @(  @   n(     �(     �(     �(  %   �(     )     *)     @)     T)     p)     �)     �)     �)     �)     �)     *     &*     C*     X*     o*  "   �*  4   �*     �*     �*     �*     +  /   $+  2   T+     �+     �+     �+  &   �+  3   �+     ,  !   ,     0,     N,     d,     i,  '   ~,     �,     �,     �,  #   �,     -     -  �   -  !   �-     �-     �-     .     1.  %   N.  "   t.     �.  %   �.     �.  %   �.     /     +/     >/     Y/     l/     �/     �/  #   �/  4   �/     0     +0     F0  !   b0     �0     �0  *   �0     �0      1     #1     21     I1  	   Z1     d1      �1     �1     �1     �1     �1     �1     �1      2     2  -   2  %   L2     r2  <   �2     �2     �2     �2     �2     �2     3     '3     83     O3     f3     v3     3     �3     �3     �3     �3     �3     �3     �3     �3     �3     4     ,4  O   F4     �4     �4     �4     �4     �4     5     5     #5     /5     ?5     M5  
   Y5     d5     v5  
   �5  
   �5     �5     �5     �5     �5  
   �5  
   �5     �5     �5     �5  9   6  2   @6     s6  &   �6  "   �6     �6     �6     	7     #7  .   17  2   `7  "   �7  4   �7  /   �7  #   8  $   ?8  )   d8  	   �8     �8     �8     �8  P   �8     9  >   19     p9     �9     �9     �9     �9     �9  !   �9  
   �9     �9     :     :  #   :  s   7:     �:     �:     �:     �:     �:     �:  &   ;  '   5;  '   ];     �;     �;     �;     �;     �;  +   �;     <     /<     A<     W<     u<     �<     �<  "   �<     �<     �<     =      -=  &   N=     u=     �=     �=     �=     �=  h   �=     7>     E>     J>     O>     W>     \>     a>  	   m>     w>     �>  !   �>    �>     �?  %   @     *@  
   2@     =@  '   I@     q@     �@  ,   �@     �@     �@  ,   �@  z   A     �A  	   �A     �A     �A     �A  	   �A     �A     �A     �A     �A  0   �A     #B  #   2B  |   VB  -   �B     C     C     %C     +C     2C  	   8C  H   BC  *   �C  #   �C  a   �C     <D  ,   ID     vD     �D     �D     �D     �D     �D     �D     �D     �D  *   �D     &E     8E  !   ME     oE  	   vE     �E     �E     �E  7   �E     F     F     %F     ?F  !   ]F  $   F  +   �F     �F     �F  	   �F  :   G  0   @G  !   qG     �G     �G  	   �G     �G  "   �G     �G  !    H  #   "H     FH     aH     |H  "   �H  &   �H     �H     �H     I  $   I     9I     XI     tI     �I     �I  9   �I  D   �I     AJ     TJ     mJ     �J     �J     �J  (   �J     �J     K  $    K  "   EK     hK  !   �K     �K  -   �K     �K     L     L     #L  *   6L     aL     }L  /   �L     �L     �L     �L     M     "M     %M     EM     ^M     lM     ~M     �M     �M     �M     �M     �M     �M     �M     N     N     (N     <N     UN  '   eN     �N     �N  -   �N  -   �N     O  %    O     FO  &   VO     }O     �O  !   �O  "   �O  !   �O  &   
P     1P     DP     WP     lP     }P     �P  
   �P  �  �P  �   jR  9   S     RS  _   ^S     �S  	   �S     �S     �S  S   �S     >T  3   MT  	   �T     �T     �T  6   �T  -   �T  (   U  /   =U  !   mU     �U  %   �U  ;   �U  J   V  P   SV  .   �V     �V     �V  ;   �V  X   /W  !   �W  5   �W  C   �W     $X     5X     SX  *   pX  9   �X  V   �X     ,Y     HY     `Y  +   xY     �Y     �Y     �Y  !   �Y  $   !Z  #   FZ  2   jZ      �Z     �Z     �Z     �Z  !   [     -[     K[     a[  &   [  /   �[     �[     �[     �[     \  9   \  <   X\     �\     �\     �\  8   �\  3   �\  	   "]     ,]     H]     b]     y]     �]  '   �]     �]     �]     �]  1   ^     L^     T^  �   d^  &   	_  !   0_  (   R_      {_  '   �_  2   �_  (   �_  '    `  )   H`  &   r`  1   �`     �`  &   �`  &   a     .a     Ha     da  '   �a  2   �a  4   �a     b  (   2b  3   [b  %   �b  +   �b  +   �b  D   c     Rc  *   oc     �c     �c     �c     �c  #   �c  $   d     0d     Gd     Ld     Td     pd     �d     �d     �d  1   �d  %   �d     e  q   (e     �e     �e     �e  !   �e  !   �e      f     #f  #   5f  (   Yf     �f     �f     �f     �f     �f     �f     �f     �f     �f  $   �f     g     g  +   ;g  (   gg  M   �g     �g     �g     h     h     9h     Uh     qh     h     �h     �h     �h  
   �h     �h     �h  
   �h  
   �h     �h     i     i     i  
   (i  
   3i     >i     Ji     Vi  B   bi  :   �i     �i  :   �i  7   8j     pj     �j     �j     �j  S   �j     'k  /   Gk  >   wk  ?   �k  )   �k  *    l  -   Kl  
   yl     �l     �l     �l  M   �l     m  W   /m     �m  .   �m     �m     �m     �m     n  (   n  
   <n     Gn     an  	   hn  ,   rn  q   �n     o     (o     ,o     2o     Io     Wo     po     �o     �o     �o      �o     �o     p  #   *p  +   Np     zp     �p  '   �p  *   �p     q     &q     Bq  +   Vq     �q     �q     �q  #   �q  )   �q     r     )r     ;r     Yr     or  t   vr     �r     �r     �r     s  
   
s  	   s     s     .s  &   6s     ]s  +   }s    �s     �t  '   �t     u     u  
   "u  '   -u     Uu     hu  '   �u  !   �u     �u  +   �u  Z   v     qv     �v  	   �v     �v     �v  
   �v  	   �v     �v     �v     �v  V   �v     =w  $   Nw  �   sw  4   x     <x  !   Rx     tx     yx     x     �x  D   �x  &   �x  !   �x  j    y     �y  -   �y     �y     �y     �y     �y      �y  $   z     Bz     Zz  %   sz  2   �z     �z     �z  #    {     ${     3{  '   F{  )   n{  (   �{  C   �{     |     |     <|     W|  6   g|  :   �|  =   �|     }  .   -}     \}  L   h}  /   �}  .   �}     ~     ~  	   2~     <~  "   O~     r~  !   �~  %   �~  (   �~     �~       &   2  8   Y     �     �     �     �  (   �     /�  )   O�  )   y�     ��  0   ��  ?   �     (�     6�     B�     O�     i�     ��  &   ��  )   ��     �  +    �     ,�  7   L�  )   ��     ��  6   ʂ  )   �     +�     F�     N�  *   e�     ��     ��  8   ��  &   �  &   �  $   6�     [�     o�     t�     ��     ��     ��  &   ф     ��     �     +�     ?�     ]�     a�  !   y�     ��     ��     ��     ʅ     �  ,   ��     *�     E�  0   \�  7   ��     ņ  $   ߆     �  -   �  !   E�     g�  %   l�  &   ��  %   ��  0   ߇     �     %�     >�     X�     q�     v�  	   ��            :   f  �    ?  �      #   �   �   �   �   /     x  D  $  L  �   �   T  *   �               �             �              r         �      �   �   �     C                    -                     �   �   �   �     _         U   �       s   �   �  �   =   �  t  J      p  �  �  �   "   j    �  2       |      *  ]                 �   �   o   z                  �           5    h    
   �  �  w  Q  �   O  �        `      K      I   y   �   F  "  n  �  z   V   :  �   �   �  1   q  �   �   �     _      @  k  F   @   �   �    ~       �  �   �          �   m  #  g   �       ,     {    J       �   �       3     ~  �   P   !  S       a   �       U  �   �             �   �         e       f       u   )  .   <  �   �   �       >      >   �    +         	  9   �      I  �   �       �       �       �   �   Z  �  �      v  �           �               �   ;   |   B         b  (   �     �  �      a  �   �  �   �   �       c           W      }   p       v   �  �       �  �   �  �               �   �       l   Y     �   �      �   �       5       �       X   �                      �           �  �   +   S  �  M      Z   �   D   W   �       O       o  8      %   q           /        x   &  �        �   �      �       .  �  E   H  �     g    �   �   �   9  �   Q   �   u                �   M         E  )   �       �   �       0   R   (  �  �   h       %  �   �       }  �   ^   2  �               �   �  Y      8       !   '  �                 K   G  �   �      �   �   6   4  �   r   ^  ]      �      �          �          A   C      �                  �               d   �   	   s      y  w       �   k   {   7   �      ;    �  N  <   i      �   1      �      L       
  �   A  �      \      �  0  �   �       [   �   `  e      �   �       H         �      c      �   �     \          '   t   �  �        X  7  m   �   3               �   �   T   -  B          6  [      �       �       n   d     �        b   �   =  P      �     V  �   ?   �   G   j   R  �           �      &       4   N   ,   l          �  $   �   �   �   i   �    

Debug flags are:
  g - Mime.types parser traces
  l - Mime.types lexical analyzer traces
  0-9 - Set debugging level
 
(Interrupt -- one more to kill letter) 
Interrupt 
Please use GNU long options instead.
Run %s --help for more info on these.
   or:    switches are:
  [OPTION...]  at %s  has %4lu message  (%4lu-%4lu)  has %4lu messages (%4lu-%4lu)  has no messages  msg part type/subtype              size  description
  near %s  near end "%s": not a group %.*s: ARGP_HELP_FMT parameter must be positive %.*s: ARGP_HELP_FMT parameter requires a value %.*s: Unknown ARGP_HELP_FMT parameter %4lu message   %4lu messages  %c is not a valid debug flag %c%s requires an argument %d: not a header line %lu new message %lu new messages %lu read message %lu read messages %lu unread message %lu unread messages %lu: Inappropriate message (has been deleted) %s %s is unknown %s [switches] %s
 %s is unknown. Hit <CR> for help %s message number collected.
 %s message numbers collected.
 %s takes only one argument %s's %s has wrong permissions %s's %s is not owned by %s %s: 0 messages
 %s: Too many arguments
 %s: ambiguous abbreviation %s: cannot get message body: %s %s: mailbox quota exceeded for this recipient %s: message would exceed maximum mailbox size for this recipient %s: no such user %s: unknown attribute %s: unknown function %s:%d: INTERNAL ERROR (please report) %s:%d: bad format string %s:%d: malformed line %s:%d: syntax error %s:%d: unknown variable: %s %s:%d: wrong datatype for %s %s:%lu: comment redefined %s:%lu: content id redefined %s:%lu: description redefined %s:%lu: missing %c %s:%lu: missing filename %s:%lu: missing subtype %s:%lu: no such message: %lu %s:%lu: syntax error %s:%lu: unmatched #end %s:mailbox `%s': %s: %s (PROGRAM ERROR) No version known!? (PROGRAM ERROR) Option should have been recognized!? (continue)
 (others) -- Local Recipients -- -- Network Recipients -- --add requires at least one --sequence argument --delete requires at least one --sequence argument . ADDRESS APOP failed for `%s' APOP user %s tried to log in with USER ARGP_HELP_FMT: %s value is less than or equal to %s Actions are: Already connected to the database Argument not applicable for z Authentication failed BOOL Bad address `%s': %s Bad arguments for the scrolling command Bad column address Bad format string Bad number of pages Badly formed file or directory name COMMAND CONTENT CRITICAL ERROR: Folder `%s' left in an inconsistent state, because an error
occurred while trying to roll back the changes.
Message range %s-%s has been renamed to %s-%s. Cannot append message to `%s': %s Cannot append message: %s Cannot create header list: %s Cannot create header: %s Cannot create mailbox %s: %s Cannot create output mailbox `%s': %s Cannot create temporary header: %s Cannot decode line `%s': %s Cannot determine my email address: %s Cannot determine my username Cannot determine sender name (msg %d) Cannot execute Cannot get homedir Cannot get message %lu: %s Cannot open %s: %s Cannot open `%s': %s Cannot open file %s: %s Cannot open mailbox %s: %s Cannot open output mailbox `%s': %s Cannot parse address `%s' (while expanding `%s'): %s Cannot parse address `%s': %s Cannot read mailbox %s: %s Cannot stat output file: %s Cannot unencapsulate message/part Child terminated abnormally: %d Command exited with status %d
 Command not allowed in an escape sequence
 Command terminated
 Command terminated on signal %d
 Common options Compatibility syntax:
 Component name:  Confirm : Conflict with previous locker Connection closed by remote host Creating mailer %s DATE DIR DNS name resolution failed Destroying the mailer Display options Disposition? Do you need help Do you want a path below your login directory Do you want the standard MH path "%s" Don't use the draft. Draft "%s" exists (%s byte).
 Draft "%s" exists (%s bytes).
 EDITOR EMAIL Edit again? Empty virtual function End of Forwarded message End of Forwarded messages Executing %s...
 External locker failed External locker killed Extra arguments FACILITY FIELD FILE FILE [FILE ...] FILE must be specified FLAGS FOLDER FORMAT File %s already exists. Rewrite File check failed Finished packing messages.
 Fixing global sequences
 Fixing private sequences
 Folder                  # of messages     (  range  )  cur msg   (other files)
 Folder %s  %s
 Folder contains  Folder contains no messages. Format string not specified Forwarded message
 Forwarded messages
 Found in %s
 GNU MH anno GNU MH fmtcheck GNU MH folder GNU MH forw GNU MH inc GNU MH install-mh GNU MH mark GNU MH mhl GNU MH mhn GNU MH mhpath GNU MH pick GNU MH refile GNU MH repl GNU MH rmf GNU MH rmm GNU MH scan GNU MH send GNU MH whom GNU messages -- count the number of messages in a mailbox GNU popauth -- manage pop3 authentication database GSS-API error %s (%s): %.*s GSSAPI user %s is NOT authorized as %s GSSAPI user %s is authorized as %s Garbage in ARGP_HELP_FMT: %s Getting message %s Getting message numbers.
 HEADER: VALUE Held %d message in %s
 Held %d messages in %s
 I'm going to create the standard MH path for you.
 INTERNAL ERROR: Unknown opcode: %x INTERNAL ERROR: unexpected item type (please report) INTERNAL ERROR: unknown argtype (please report) Incorrect value for decode-fallback Input string is not RFC 2047 encoded Internal error: condition stack underflow Interrupt Invalid command Invalid header: %s LIST List the addresses and verify that they are acceptable to the transport service. List the draft on the terminal. List the message being distributed/replied-to on the terminal. Listing options Lock file check failed Lock not held on file Locker null MAILER MBOX MBOX environment variable not set MHL-FILTER MIME editing options MINUTES MSG Malformed or unsupported mailer URL Mandatory or optional arguments to long options are also mandatory or optional for any corresponding short options. Message contains:
 NAME NUMBER New mail has arrived.
 No applicable message No applicable messages No fields are currently being ignored
 No fields are currently being retained
 No fields are currently being unfolded
 No mail for %s
 No messages in that folder!
 No new mail for %s
 No previous file No query was yet executed No result from the previous query available No such interface No such user name No value set for "%s" Not a valid RFC 2047 encoding Not connected to the database Not enough buffer space Not enough memory Not enough memory to edit the line Old Password: On first screenful of messages
 On last screenful of messages
 Only one SCRIPT can be specified Only the file owner can use --username Opening mailer %s Operation canceled Operation failed Operation not allowed Options are: Options marked with `*' are not yet implemented.
Use -help to obtain the list of traditional MH options. Other options PART PATH PATTERN PERM PROG Parse error Password: Passwords differ. Please retry. Please answer yes or no:  Press <return> to show content... Prior to using MH, it is necessary to have a file in your login
directory (%s) named .mh_profile which contains information
to direct certain MH operations.  The only item which is required
is the path to use for all MH folder operations.  The suggested MH
path for you is %s...
 Process exited on signal Process exited with a non-zero status RETRIES Read error Reading %s
 Refile the draft into the given FOLDER. Remove folder %s Renaming %s to %s
 Replace the draft with the newly created one Report bugs to %s.
 Requested item not found Result of the previous query is not released Returns 0 on success, 3 if locking the file fails because it's already locked, and 1 if some other kind of error occurred. Reverting pack table.
 Run `%s'? SCRIPT SECONDS SECS SQL error STATUS STRING SUBJ SWITCHES Saved %d message in %s
 Saved %d messages in %s
 Saving options Send the message in the background. Send the message. The -watch flag causes the delivery process to be monitored. SWITCHES are passed to send program verbatim. Sender address is obtained from the envelope
 Sending message %s Set output width Sorry TICKET TOTAL TYPE-LIST Terminate the session. Preserve the draft, unless -delete flag is given. Terminate the session. Preserve the draft. There are messages in that folder.
 There is %lu message in your incoming mailbox.
 There are %lu messages in your incoming mailbox.
 This is %s

 Too many arguments for the scrolling command Trying %s...
 URL USER USERNAME Unexpected eof on input Unfinished escape Unknown command: %s Unknown command: %s
 Unknown escape %s Unknown failure while executing subprocess Unknown mode `%s' Unknown system error Unsupported authentication scheme Usage: Use "%s"? Use "." to terminate letter. Use "quit" to quit. Use "~." to terminate letter. Use -help to obtain the list of traditional MH options. Use the draft. Use this draft User name is not supplied User password is not supplied Valid if arguments are: s | r | t Variable sendmail not set: no mailer Warning: your .biffrc has wrong permissions What is the full path?  What is the path?  What now? You already have an MH profile, use an editor to modify it You may not specify more than one `-aldp' option You will have to fix it manually. [FILE] [OPTIONS] [USER] [URL ...] [mailbox...] `%s' already included at top level `%s' already included here action `%s' has not been required cannot change to directory `%s': %s cannot create arg list: %s cannot create iterator: %s cannot create stack cannot create temporary stream: %s cannot encode subject using %s, %s: %s cannot get address! cannot get filename! cannot get text! cannot parse date specification (%s) cannot rename `%s' to `%s': %s cannot retrieve argument %d cannot retrieve field %lu: %s cannot save to mailbox: %s cannot stat `%s': %s comparator %s is incompatible with :count in call to `%s' comparator `%s' is incompatible with match type `%s' in call to `%s' delivering into %s else without matching if endif without matching if expected %s but passed %s extra arguments format: divide by zero fribidi failed to recognize charset `%s' give a short usage message give this help list hang for SECS seconds (default 3600) if requires an argument: s | r | t in %4lu folder in %4lu folders inbox-url destfile [POP-password] invalid data type invalid relational match `%s' in call to `%s' invalid tag name `%s' for `%s' mailbox `%s': %s: %s major marking as deleted match type specified twice in call to `%s' messages specific switches: minor missing closing quote in preprocessor statement mu_authority_set_ticket failed mu_folder_get_authority failed mu_mailbox_get_folder failed mu_mailbox_scan: %s nN no alternate message to display no draft file to display no lines out
 no messages in %s no messages in range %s no next message no prev message not enough memory only one message at a time! oops? preprocessor syntax print program version private range error recursive inclusion reject: cannot get text! required action required argument for tag %s is missing required comparator required test second argument cannot be converted to number second argument must be a list of one element set the program name source for the %s %s is not available stack underflow storing message %s part %s as file %s
 test `%s' has not been required to %s too few arguments in call to `%s' too many arguments in call to `%s' unable to open temporary file: %s unalias requires at least one argument undefined function unknown action: %s unknown message type unknown test: %s yY | Encoding=%s
 | Type=%s
 Project-Id-Version: mailutils 1.1
Report-Msgid-Bugs-To: bug-mailutils@gnu.org
POT-Creation-Date: 2012-06-06 07:24+0300
PO-Revision-Date: 2006-12-29 14:15+0200
Last-Translator: Sergey Poznyakoff <gray@gnu.org>
Language-Team: Russian <ru@li.org>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=KOI8-R
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
 

����� �������:
  g - ����������� ��������������� ����������� ����� mime.types
  l - ����������� ������������ ����������� ����� mime.types
  0-9 - ��������� ������ �������
 
(���������� -- ����� �������� ���������, ����� ��� ����) 
���������� 
������� ������������ ������� ����� GNU.
������� %s --help ��� ��������� ��������� ����������.
   ���:    �����:
  [�����...]  � ������� %s  %4lu ��������� (%4lu-%4lu)  %4lu ��������� (%4lu-%4lu)  %4lu ��������� (%4lu-%4lu)  ��� ���������  ��� ����� ���/������               ����. ��������
  ����� %s  ����� ����� ������ "%s": �� ������ %.*s: �������� ARGP_HELP_FMT ������ ���� ������������� %.*s: �������� ARGP_HELP_FMT ������� �������� %.*s: ����������� �������� ARGP_HELP_FMT %4lu ���������  %4lu ���������  %4lu ���������  ���������������� ���� �������: %c %c%s ������� ��������� %d: ���������������� ������ ��������� %lu ����� ��������� %lu ����� ��������� %lu ����� ��������� %lu ����ԣ���� ��������� %lu ����ԣ���� ��������� %lu ����ԣ���� ��������� %lu ������ԣ���� ��������� %lu ������ԣ���� ��������� %lu ������ԣ���� ��������� %lu: ���������������� ��������� (���� �������) %s %s ���������� %s [�����] %s
 ������� %s ����������. ������� <CR> ��� ��������� ��������� ������ %s ����� ���������.
 ������� %s ������ ���������.
 ������� %s ������� ���������.
 %s ��������� ������ ���� �������� �������� ����� ������� �� ���� %2$s ������������ %1$s ������������ %s �� �������� ���������� ����� %s ��� ������������ %s %s: 0 ���������
 %s: ������� ����� ����������
 %s: ������������� ���������� %s: ���������� �������� ���� ���������: %s %s: ������������ �������� ����� �� ������ ��������� ����� %s: ����� ��������� �������� �� ������������ ������ ��������� ����� ��� ����� �������� %s: ��� ������ ������������ %s: ����������� ������� %s: ����������� ������� %s:%d: ���������� ������ (�������� �������) %s:%d: �������� ������ ������� %s:%d: ���������������� ������ %s:%d: �������������� ������ %s:%d: ����������� ����������: %s %s:%d: ����������� ��� ������ ��� %s %s:%lu: ��������������� ����������� %s:%lu: ��������������� �������������� ����������� %s:%lu: ��������������� �������� %s:%lu: ��������� %c %s:%lu: ��������� ����� ����� %s:%lu: ��������� ������� %s:%lu: ��� ������ ���������: %lu %s:%lu: �������������� ������ %s:%lu: �������� #end %s:�������� ���� `%s': %s: %s (������ ���������) ������ ����������!? (������ ���������) ����� ��������� ����������!? (�����������)
 (������) -- ��������� �������� -- -- ������� �������� -- ����� --add ������� �� ������� ���� ���� ����� --sequence ����� --delete ������� �� ������� ���� ���� ����� --sequence . ����� APOP �� ������ ��� `%s' ������������ APOP %s ��������� �������������� ����� USER ARGP_HELP_FMT: �������� %s ������ ��� ���� ����� %s ��������: ��� ��������� � ���� ������ �������� ���������� ��� z ����������� �� ������� [yes ��� no] ���������������� ����� `%s': %s �������� ��������� ��� ������� �������� �������� ����� ������� �������� ������ ������� ��������� ���������� ������� ����������� �������������� ��� ����� ��� �������� ������� ���-����������� ����������� ������: ������� `%s' �������� � ������������ ��������� ��-�� ������, ��������� ��� ������� ������ ���������.
��������� %s-%s ���� ������������� � %s-%s. ���������� �������� ��������� `%s': %s ���������� �������� ���������: %s ���������� ������� ������ ����������: %s ���������� ������� ���������: %s ���������� ������� �������� ���� %s: %s ���������� ������� �������� �������� ���� `%s': %s ������ �������� ���������� ���������: %s ���������� ������������ ������ `%s': %s ���������� ���������� ��� ����� email: %s ���������� ���������� ��� ������������ ���������� ���������� ��� ����������� (�����. %d) ���������� ��������� ���������� ���������� �������� ������� ���������� ��������� ��������� %lu: %s ���������� ������� %s: %s ���������� ������� `%s': %s ���������� ������� ���� %s: %s ���������� ������� �������� ���� %s: %s ���������� ������� �������� �������� ���� `%s': %s ������ ������� ������ `%s' (��� ���������� `%s'): %s ������ ������� ������ `%s': %s ���������� �������� �������� ���� %s: %s ���������� �������� ���������� � �������� �����: %s ���������� ���������� ���������/����� �������� ������� ���������� �����������: %d �������� ������� ���������� �� �������� %d
 ����� ���� ������� �� �������� �� �������������� ������������������
 �������� ������� ����������
 �������� ������� ���������� �� ������� %d
 ����� ����� ��������� ������������� � MH:
 ��� ����������:  ������� ��� ���: �������� � ���������� ������������� ���̣���� ������ �������� ���������� �������� ���������� %s ���� ������� ����� ����� �� DNS �������� ����������� ���������� ����� ������ �� ����� ����������? ����� ������ ������������ ���� ������������ ��������� �������� ������������ ����������� ���� MH "%s" �� ������������ ��������. �������� "%s" ���������� (%s ����).
 �������� "%s" ���������� (%s �����).
 �������� "%s" ���������� (%s ������).
 �������� EMAIL ������������� �����? ����������� ������� �� ���������� ����� ����������������� ��������� ����� ���������������� ��������� ���������� %s...
 ������ ������� ��������� ���������� ������� ��������� ���������� ����������� �������������� ��������� ����� ���� ���� ���� [���� ...] ������� ���� ����� ������� ������ ���� %s ��� ����������. ������������ �������� ����� �� ������� �������� ��������� ���������.
 ����������� ���������� �������������������
 ����������� ������� �������������������
 �������                 �-�� ���������    (�������� )  ���.����. (��. �����)
 ������� %s  %s
 � �������� ����� ����  � �������� ��� ���������. ������ ������� �� ������� ���������������� ���������
 ���������������� ���������
 ������� � %s
 GNU MH anno GNU MH fmtcheck GNU MH folder GNU MH forw GNU MH inc GNU MH install-mh GNU MH mark GNU MH mhl GNU MH mhn GNU MH mhpath GNU MH pick GNU MH refile GNU MH repl GNU MH rmf GNU MH rmm GNU MH scan GNU MH send GNU MH whom GNU messages -- ������������ ���������� ��������� � �������� ����� GNU popauth -- ����������������� ��������������� ���� pop3 ������ GSS-API %s (%s): %.*s ������������ GSSAPI %s �� ������� � ������������� ����� %s ������������ GSSAPI %s ������� � ������������� ����� %s ����� � ARGP_HELP_FMT: %s ��������� ��������� %s ��������� ������� ���������.
 ���������: �������� %d ��������� �������� � %s
 %d ��������� �������� � %s
 %d ��������� �������� � %s
 �������� ������������ ���� MH.
 ���������� ������: ����������� ��� ��������: %x ���������� ������: ����������� ��� �������� (�������� �������) ���������� ������: ����������� ��� ��������� (�������� �������) ���������������� �������� decode-fallback ������� ������ �� ������������ �� RFC 2047 ���������� ������: ������������ ����� ������� ���������� ���������������� ������� ���������������� ���������: %s ������ ������� ������ ������� � ��������� �� ������������ ��� ������������� �������. ������� �������� �� ��������. ������� �� �������� ����� ���������, ������� ������������ ��� �� ������� ������� �����. ����� ������ �������� �������� ����� ���������� ����������� �������� ���� �� ������������ ������� ����������� ���������-����� ��������-���� �� ����������� ���������� ��������� MBOX ������-MHL ����� �������������� MIME ������ ��������� �������� ��� ���������������� URL ���������� ������������ ��� �������������� ��������� � ������� ������ �������� �������� � � ��������������� �������� ������. ���������� ���������:
 ��� ����� �������� ����� �����.
 ��� ��������� ��� ���������� ��������� ������� ���� �� ������������
 ������� ���� �� ������������
 ������� ���� �� �����������
 ��� ����� ��� %s
 � �������� ����� ��� ���������!
 ��� ����� ����� ��� %s
 ��� ����������� ����� ���� �� ��������� �� ������ ������� ����������� ��������� ����������� ��������� ��� ������ ����� ���������� ��� ������ ����� ������������ �� ����������� �������� ���������� "%s" ��� - �� �������������� ��������� RFC 2047 �ݣ �� ��������� � ���� ������ ������������ ����� � ������ ������������ ������ ��� �������������� ������ �� ������� ������ ������ ������: ������ ����� ���������
 ��������� ����� ���������
 ����� ������� ������ ���� ��������� ������ �������� ����� �������� --username �������� ���������� %s �������� �������� �������� ����������� �������� �������� �� ��������� �����: �����, ���������� ������ '*' ��� �� �����������.
����������� ����� -help ��� ��������� ������ ������������ ����� MH. ������ ����� ����� ���� ������ ���������� ��������� ������ ������� ������: ������ ����������. ���������� ��� ���. ������� �������� 'yes' ��� 'no' ��� ������ �����������, ������� <return>... ����� �������������� MH, � ����� �������� �������� (%s) ����� ������ ���� � ������ .mh_profile, ������� ����� ��������� ����������, ����������� ���������� ���������� MH. ������������ ������������ ������� � ���� ����� -- ���� � �������� ��������� �������� MH. ������������ ����: %s...
 ������� ���������� �� ������� ������� ���������� � ��������� �������� ������� ������ ������ ������ %s
 ��������� �������� � �������� ��������. ������� ������� %s �������������� %s � %s
 �������� �������� ������ ��� ���������. �� ������� ������� �� ������ %s.
 �� ������ ������������� ������� ��������� ����������� ������� �� �������ģ� ���������� 0 ��� �������� ����������, 3 - ���� ���� ��� ������������, 1 - � ������ ������. �������� ������� ��������.
 ��������� `%s'? ��������� ������� ������� ������ SQL ��������� ������ ���� ������������� %d ��������� ��������� � %s
 %d ��������� ��������� � %s
 %d ��������� ��������� � %s
 ����� ���������� �������� ��������� � ������� ������. �������� ���������. ���� ������� ����� -watch, �������� ��������� ���������� � �������� ��������. ����� ���������� ��������� ������� ��� ���������. ������ ����������� ���������� �� �������� ���������
 �������� ��������� %s ���������� ������ ������ �� ����� ���� ����� ����� ������-����� ��������� ������. ���� �� ������� ����� -delete, ��������� ��������. ��������� ������ � ��������� ��������. ���� ��������� � �������� �����.
 � ����� �������� ����� %lu ������.
 � ����� �������� ����� %lu ������.
 � ����� �������� ����� %lu �����.
 �������� ��� %s

 ������� ����� ���������� ��� ������� �������� ����� � %s...
 URL ������������ ���_������������ ����������� ����� ����� �� ����� ������������� �������������� ������� ����������� �������: %s ����������� �������: %s
 ����������� �������������� ������� %s ����������� ������ �� ����� ���������� ����������� ����������� ����� `%s' ����������� ��������� ������ ����� ����������� �� �������������� �������������: ������������ "%s"? ����������� "." ����� ��������� ������. ����������� "quit" ��� ���������� ������. ����������� "~." ����� ��������� ������. ����������� ����� -help ��� ��������� ������ ������������ ����� MH. ������������ ��������. ������������ ��������� �������� �� ������ ��� ������������ �� ����� ������ ��������������� ����������� ��� if ��������: s | r | t ���������� sendmail �� �����������: �� ������̣� ��������� ��������������: � ������ ����� .biffrc �������� ����� ������� ������� ������ ����:  ������� ���� ������������ ��������� ��������:  ��� ������? � ��� ��� ���� .mh_profile, ������������� ��� � ������� ���������� ��������� ����� ���� ������� ������ ���� �� ����� `-aldp' ��� �������� ��������� �������������� �������. [����] [�����] [������������] [URL ...] [�������� ����...] `%s' ��� ������� �� ������� ������ '%s' ��� ������� ����� �������� '%s' �� ���� ����������� �� ������� ������� � ������� `%s': %s ���������� ������� ������ ����������: %s ���������� ������� ��������: %s ���������� ������� ���� ���������� ������� ��������� �����: %s ���������� ������������ ���� � �������������� %s, %s: %s ���������� �������� �����! ���������� �������� ��� �����! ���������� �������� �����! ���������� ��������� ���� (%s) �� ������� ������������� `%s' � `%s': %s ���������� �������� �������� %d ���������� �������� �������� ���� %lu: %s ���������� ��������� � �������� �����: %s ������ stat(%s): %s ���������� %s � ������ '%s' ����������� � :count ���������� %s  � ������ '%s' ����������� � ����� ��������� '%s' �������� � %s esle ��� if endif ��� if ��������� %s, �������� %s �������������� ��������� format: ������� �� ���� fribidi �� ������� ����� �������� `%s' ������ ������� ��������� �� ������������� ������ ��� ��������� ��������� �� ��� ������ (�� ��������� 3600) if ������� ���������: s | r | t � %4lu-� �������� � %4lu-x ��������� � %4lu-� ��������� ��������-URL ����-���������� [������-POP] ���������������� ��� ������ ���������������� �������� ��������� `%s' � ������ `%s' ���������������� ��� ������ '%s' ��� '%s' �������� ���� `%s': %s: %s ������� �������� ��� ��������� ��� ������������ � ������ %s ������ ������ ����� messages: �������������� � ���������� ������������� ��������� ����������� ������� ������ ������� mu_authority_set_ticket ������ ������� mu_folder_get_authority ������ ������� mu_mailbox_get_folder mu_mailbox_scan: %s nN�� ��� ��������������� ��������� ��� ��������� ����� �� ����� ������ ������
 � %s ��� ��������� � ��������� %s ��� �� ������ ��������� ��� ���������� ��������� ��� ����������� ��������� ������������ ������ ������ ���� ��������� �� ���! ��? ��������� ������������� ���������� ����� ������ ��������� ������� ������ ��������� ����������� ��������� ���������� �������� �����! �������������� �������� ����������� ����������� �������� � ������ %s �������������� ����������� ������������� �������� ���������� ������������� ������ �������� � ����� ������ ���������� ������ ���� ������ �� ������ �������� ������ �������� ��������� ����������� �������� ����� ��� %s %s ������������ ����� ���������� ��������� %s, ����� %s � ����� %s
 �������� '%s' �� ���� ����������� � %s ������������ ���������� � ������ '%s' ������� ����� ���������� � ������ '%s' ���������� ������� ��������� ����: %s unalias ������� �� ������� ���� ������ ��������� ������� ������������ ����������� ��������: %s ����������� ��� ��������� ����������� ��������: %s yY�� | ���������=%s
 | ���=%s
 