��         T  a  �        '     
   9  L   D     �     �     �     �  =   �     �  6        C  	   L     V  .   h  %   �     �     �     �       -   (     V     g      y     �     �     �     �     �  -     @   ;     |  %   �     �     �     �     �          0     M     k     ~     �     �     �     �  "   �  4        P     \     e     |  /   �  2   �     �        &        <     I     _     d  '   y     �     �     �  *   �     �     	        	   1     ;     Y     ^     b     }     �     �  -   �  %   �     �               !     -     D     ]     w     �     �     �     �     �     �     �     �     �       O        f     u     �  9   �  2   �  &   	   "   0      S   .   p   2   �   "   �   4   �   /   *!  $   Z!  	   !     �!  P   �!     �!  >   �!     >"     N"     e"     {"     �"     �"  !   �"  
   �"     �"     �"     �"  #   �"  s   #     y#     �#     �#     �#     �#     �#  &   �#  '   $  '   +$     S$     c$     w$     �$     �$     �$     �$     �$     �$  &   %     6%     G%     ]%     j%     x%     }%     �%  	   �%     �%     �%  !   �%    �%     '     '  '   '     D'  ,   U'     �'     �'     �'     �'     �'     �'  0   �'     �'  #   �'  |   (     �(     �(     �(     �(  H   �(  *   )     3)  ,   @)     m)     q)     v)     )     �)     �)  !   �)     �)  	   �)     �)     
*     *     <*     K*     Z*     t*  +   �*     �*     �*  	   �*  :   �*  0   .+     _+     f+  "   s+     �+  !   �+     �+     �+  9   ,  D   @,     �,     �,     �,     �,     �,  "   �,     -  -   4-     b-     �-  *   �-     �-  /   �-     .     +.     D.     R.     d.     |.     �.     �.     �.     �.     �.     �.     �.     �.     /     /     0/  -   >/  -   l/  %   �/     �/  &   �/     �/     0  !   0     ?0  "   R0     u0  !   �0  &   �0     �0     �0     �0     1  
   "1  �  -1  =   �2     3  l   '3     �3     �3     �3     �3  :   �3      4  4   4     C4     Q4     d4  1   w4  (   �4     �4  !   �4     5     '5  $   C5     h5     ~5  .   �5     �5     �5  -   �5      6     .6  <   H6  J   �6     �6  *   �6     7     )7      B7  *   c7     �7     �7     �7     �7  !   �7     8     48     S8     m8  /   �8  8   �8     �8     9     9     $9  +   ;9  2   g9     �9     �9  3   �9     �9      :     :     :  -   2:     `:     |:     �:  /   �:     �:     �:     �:     ;     ;     1;     6;     :;     Z;     m;     y;  5   �;     �;     �;     �;     �;     <     <  $   )<  &   N<     u<      �<  
   �<     �<     �<     �<     �<     �<     �<  "    =     #=  P   B=     �=     �=     �=  ?   �=  ?   >  (   [>  %   �>  #   �>  9   �>  (   ?  %   1?  >   W?  E   �?  7   �?     @      @  M   )@     w@  H   �@     �@  &   �@  '   A     ?A     KA     RA  &   WA  
   ~A     �A     �A     �A  #   �A  �   �A     dB     vB     {B     �B     �B     �B  $   �B  $   �B  .   C     =C     UC     sC     �C  !   �C     �C     �C  %   �C  &   D  ;   8D     tD     �D     �D     �D     �D     �D     �D     �D  &   �D      �D  -   E  @  DE     �F     �F  +   �F     �F  -   �F     G     (G     /G     7G     ;G  
   CG  7   NG     �G     �G  �   �G     ?H     [H     gH     nH  Q   tH  #   �H     �H  0   �H     ,I     0I     5I     EI     ]I     vI  #   �I  	   �I     �I  )   �I     �I  *   J     =J     TJ     mJ  "   �J  4   �J     �J     �J     K  ?   K  1   XK     �K     �K  %   �K     �K     �K     �K     L  B   9L  U   |L     �L     �L     �L     M     'M  "   AM     dM  :   �M  +   �M     �M  E   �M     ?N  8   _N  $   �N  !   �N     �N     �N     O     /O     FO     ]O     uO     �O     �O     �O     �O     �O     �O     �O  
   P  6   P  7   CP  &   {P  $   �P  /   �P     �P     Q  ,   Q     IQ  +   ^Q  !   �Q  *   �Q  &   �Q     �Q     R     )R     AR  	   UR     �           �       �   �   �       �   	       �       #   �       �       �   �      ~       j   �             �     `   +   �          x               �   _              �   �      �       �   ^   �       {   �   �   �   G   W   H   �   �       '           2   �             ;   L           
           �   v   q   n   &       �   Z   }       �   ]       B       z       �   R   �   �   �   U   �       �   �   �   y   �   �   �   �   !           k   F      D      t   �   �   �       �          $   N   �       4   �   �       K        �   l                    �              C       �   �   3   |   A          �   �           �       �   �   �   �         Y   c       �   �   �     g   �   �   i   �      �       �      �   �   X   �   p   �      �         �   s           �         �   �      :   %       u       �           �       �   �           /       J   �           5   �               �   �   �   o   *   �   �   .   S      6      ,           )   �   �   (   w   @   9   Q      �   �   m          8   �   �   �   �   \   "   �       0   e   �       �   >   ?   �   �   b   r       �   �       T   f   [   �   7   �       �      V   �   �   h   =   �   I   �   �   -   �       �       �   �          �   �               a           <   M      �   1   �       E   �       �       d   �       �   �   �   P       O         �       �   �      �           
(Interrupt -- one more to kill letter) 
Interrupt 
Please use GNU long options instead.
Run %s --help for more info on these.
   or:    switches are:
  [OPTION...]  at %s  has %4lu message  (%4lu-%4lu)  has %4lu messages (%4lu-%4lu)  has no messages  msg part type/subtype              size  description
  near %s  near end "%s": not a group %.*s: ARGP_HELP_FMT parameter requires a value %.*s: Unknown ARGP_HELP_FMT parameter %4lu message   %4lu messages  %c is not a valid debug flag %c%s requires an argument %d: not a header line %lu: Inappropriate message (has been deleted) %s %s is unknown %s [switches] %s
 %s is unknown. Hit <CR> for help %s on msg uid %lu %s's %s has wrong permissions %s's %s is not owned by %s %s: 0 messages
 %s: Too many arguments
 %s: mailbox quota exceeded for this recipient %s: message would exceed maximum mailbox size for this recipient %s: no such user %s:%d: INTERNAL ERROR (please report) %s:%d: malformed line %s:%d: syntax error %s:%d: unknown variable: %s %s:%d: wrong datatype for %s %s:%lu: comment redefined %s:%lu: content id redefined %s:%lu: description redefined %s:%lu: missing %c %s:%lu: missing filename %s:%lu: missing subtype %s:%lu: no such message: %lu %s:%lu: syntax error %s:%lu: unmatched #end (PROGRAM ERROR) No version known!? (PROGRAM ERROR) Option should have been recognized!? (continue)
 (others) -- Local Recipients -- -- Network Recipients -- --add requires at least one --sequence argument --delete requires at least one --sequence argument ADDRESS APOP failed for `%s' APOP user %s tried to log in with USER Actions are: Authentication failed BOOL Bad address `%s': %s Bad arguments for the scrolling command Bad format string Bad number of pages CONTENT Command not allowed in an escape sequence
 Common options Compatibility syntax:
 Component name:  Confirm : Conflict with previous locker DATE DIR DNS name resolution failed Display options Disposition? Do you need help Do you want a path below your login directory Do you want the standard MH path "%s" Don't use the draft. EDITOR EMAIL Edit again? Empty virtual function End of Forwarded message End of Forwarded messages External locker failed External locker killed FACILITY FIELD FILE FILE must be specified FLAGS FOLDER FORMAT File %s already exists. Rewrite File check failed Folder                  # of messages     (  range  )  cur msg   (other files)
 Folder %s  %s
 Forwarded message
 Forwarded messages
 GNU messages -- count the number of messages in a mailbox GNU popauth -- manage pop3 authentication database GSSAPI user %s is NOT authorized as %s GSSAPI user %s is authorized as %s Garbage in ARGP_HELP_FMT: %s Held %d message in %s
 Held %d messages in %s
 I'm going to create the standard MH path for you.
 INTERNAL ERROR: Unknown opcode: %x INTERNAL ERROR: unexpected item type (please report) INTERNAL ERROR: unknown argtype (please report) Input string is not RFC 2047 encoded Interrupt LIST List the addresses and verify that they are acceptable to the transport service. List the draft on the terminal. List the message being distributed/replied-to on the terminal. Listing options Lock file check failed Lock not held on file Locker null MAILER MBOX MBOX environment variable not set MHL-FILTER MIME editing options MINUTES MSG Malformed or unsupported mailer URL Mandatory or optional arguments to long options are also mandatory or optional for any corresponding short options. Message contains:
 NAME NUMBER New mail has arrived.
 No applicable message No applicable messages No fields are currently being ignored
 No fields are currently being retained
 No fields are currently being unfolded
 No mail for %s
 No new mail for %s
 No previous file No such user name No value set for "%s" Not enough memory Old Password: On first screenful of messages
 On last screenful of messages
 Only the file owner can use --username Operation failed Operation not allowed Options are: Other options PART PATTERN PROG Password: Passwords differ. Please retry. Please answer yes or no:  Press <return> to show content... Prior to using MH, it is necessary to have a file in your login
directory (%s) named .mh_profile which contains information
to direct certain MH operations.  The only item which is required
is the path to use for all MH folder operations.  The suggested MH
path for you is %s...
 RETRIES Reading %s
 Refile the draft into the given FOLDER. Remove folder %s Replace the draft with the newly created one Report bugs to %s.
 SCRIPT SECONDS STRING SUBJ SWITCHES Saved %d message in %s
 Saved %d messages in %s
 Saving options Send the message in the background. Send the message. The -watch flag causes the delivery process to be monitored. SWITCHES are passed to send program verbatim. Set output width Sorry TICKET TOTAL Terminate the session. Preserve the draft, unless -delete flag is given. Terminate the session. Preserve the draft. This is %s

 Too many arguments for the scrolling command URL USER USERNAME Unknown command: %s Unknown command: %s
 Unknown escape %s Unsupported authentication scheme Usage: Use "%s"? Use "." to terminate letter. Use "quit" to quit. Use "~." to terminate letter. Use the draft. Use this draft User name is not supplied User password is not supplied Warning: your .biffrc has wrong permissions What is the full path?  What is the path?  What now? You already have an MH profile, use an editor to modify it You may not specify more than one `-aldp' option [FILE] [mailbox...] `%s' already included at top level `%s' already included here action `%s' has not been required cannot create iterator: %s cannot send message: %s comparator %s is incompatible with :count in call to `%s' comparator `%s' is incompatible with match type `%s' in call to `%s' delivering into %s else without matching if endif without matching if extra arguments format: divide by zero if requires an argument: s | r | t in %4lu folder in %4lu folders invalid relational match `%s' in call to `%s' invalid tag name `%s' for `%s' marking as deleted match type specified twice in call to `%s' messages specific switches: missing closing quote in preprocessor statement no alternate message to display no draft file to display no lines out
 no messages in %s no messages in range %s no next message no prev message not enough memory only one message at a time! oops? preprocessor syntax private range error recursive inclusion required action required comparator required test second argument cannot be converted to number second argument must be a list of one element source for the %s %s is not available stack underflow storing message %s part %s as file %s
 test `%s' has not been required to %s too few arguments in call to `%s' too many arguments too many arguments in call to `%s' too many children (%lu) unable to open temporary file: %s unalias requires at least one argument undefined function unknown action: %s unknown message type unknown test: %s | Type=%s
 Project-Id-Version: mailutils 0.4
Report-Msgid-Bugs-To: bug-mailutils@gnu.org
POT-Creation-Date: 2012-06-06 07:24+0300
PO-Revision-Date: 2003-12-10 08:55+0200
Last-Translator: Eugen Hoanca <eugenh@urban-grafx.ro>
Language-Team: Romanian <translation-team-ro@lists.sourceforge.net>
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=ISO-8859-2
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n!=1);
 
(�ntrerupere -- �nc� una pentru a distruge(kill) scrisoarea) 
�ntrerupere 
V� rug�m folosi�i �n loc op�iunile lungi GNU.
Rula�i %s --help pentru mai multe informa�ii despre acestea.
   sau:    switch-uri sunt:
  [OP�IUNE...]  la %s  are %4lu mesaj  (%4lu-%4lu)  are %4lu mesaje  (%4lu-%4lu) nu are mesaje  msj tip/subtip parte             m�rime  descriere
 aproape de %s aproape de sf�r�it "%s": nu este grup %.*s: parametrul ARGP_HELP_FMT necesit� o valoare %.*s: Parametru ARGP_HELP_FMT necunoscut %4lu mesaj   %4lu mesaje %c nu este un flag de debug valid %c%s necesit� un argument %d: nu este linie de header %lu: mesaj nepotrivit (a fost �ters) %s %s este necunoscut %s [switch-uri] %s
 %s este necunoscut. Ap�sa�i <CR> pentru ajutor %s pe msj uid %lu %s's %s are permisiuni gre�ite %s's %s nu este �n proprietatea(owned) lui %s %s: 0 mesaje
 %s: Prea multe argumente
 %s: quota maibox-ului a fost dep�it� pentru acest recipient %s: mesajul va dep�i m�rimea maxim� a mailbox-ului pentru acest recipient %s: nu exist� user-ul %s:%d: EROARE INTERN� (v� rug�m raporta�i) %s:%d: linie malformat� %s:%d: eroare de sintax� %s:%d: variabil� necunoscut�: %s %s:%d: tip dat�(datatype) gre�it pentru %s %s:%lu: comentariu redefinit %s:%lu: id con�inut redefinit %s:%lu: descriere redefinit� %s:%lu: lipse�te %c %s:%lu: lipse�te numele de fi�ier %s:%lu: lipse�te subtip %s:%lu: nu exist� mesajul: %lu %s:%lu: eroare de sintax� %s:%lu: #end f�r� corespondent (EROARE DE PROGRAM) Nici o versiune cunoscut�!? (EROARE DE PROGRAM) Op�iunea ar fi trebuit recunoscut�!? (continuare)
 (altele) -- Recipiente Locale -- -- Recipiente Re�ea -- --add necesit� minim un argument --sequence --delete necesit� cel pu�in un argument --sequence ADRES� eroare de APOP pentru `%s' User-ul APOP %s a �ncercat s� intre(log in) cu USER Ac�iunile sunt: Autentificare e�uat� BOOL Adres� gre�it� `%s': %s Argumente gre�ite pentru comanda de scrolling �irul de format este gre�it Num�r de pagini gre�it CON�INUT Comanda nu este permis� �ntr-o secven�� escape
 Op�iuni comune Sitax� de compatibiliate:
 Nume component�:  Confrimare: Conflic cu lockerul anterior DAT� DIR eroare �n rezolu�ia numelui DNS Afi�eaz� op�iunile Dispozi�ie? Dori�i ajutor Dori�i calea de sub(below) directorul vostru de login Dori�i calea MH standard "%s" Nu folosi aceast� ciorn�. EDITOR EMAIL Edita�i din nou? Func�ie virtual� vid� Sf�r�it de mesaj �naintat(forwarded) Sf�r�it de mesaje �naintate(forwarded) Eroare la locker-ul extern Locker-ul extern distrus(killed) FACILITATE C�MP FI�IER FI�IERul trebuie specificat MARCAJE(FLAGS) DOSAR FORMAT Fi�ierul %s exist� deja. Rescrie�i Verificarea fi�ierului a e�uat Dosar                  # de mesaje      (  inteval  )  msj crt   (alte fi�iere)
 Dosarul %s  %s
 Mesaj �naintat(forwarded)
 Mesaje �naintate(forwarded)
 GNU messages -- contorizeaz� num�rul de mesaje dintr-un mailbox GNU popauth -- administreaz� baza de date de autentificare pop3 userul GSSAPI %s NU este autorizat ca %s userul GSSAPI %s este autorizat ca %s Gunoi(garbage) �n ARGP_HELP_FMT: %s S-a p�strat %d mesaj �n %s
 S-au p�strat %d mesaje �n %s
 Voi crea o cale MH standard pentru voi.
 EROARE INTERN�: Opcode necunoscut: %x EROARE INTERN�: tip de element necunoscut (v� rug�m raporta�i) EROARE INTERN�: tip argument(argtype) necunoscut (v� rug�m raporta�i) �irul de intrare(input) nu este codat(encoded) RFC 2047 �ntrerupere LISTEAZ� Listeaz� adresele �i verific� dac� sunt acceptabile serviciului de transport. Listeaz� ciorna la terminal. Listeaz� mesajele care sunt distribuite/la care s-a r�spuns �n terminal. Op�iuni de listare Verificarea fi�ierului de lock a e�uat Blocare(lock) nu �inere(held) de fi�ier Locker null MAILER MBOX variabila de mediu MBOX nu este setat� FILTRU-MHL Op�iuni editare MIME MINUTE MSG URL mailer malformat sau nesuportat Argumentele obligatorii sau op�ionale pentru op�iunile lungi sunt de asemenea obligatorii sau op�ionale pentru toate op�iunile scurte corespunz�toare. Mesajul con�ine:
 NUME NUM�R A venit mail nou.
 Nici un mesaj aplicabil Nici un mesaj aplicabil Nici un c�mp nu este ignorat curent
 Nici un c�mp nu este re�inut curent
 Nici un c�mp nu este deschis(unfolded) curent
 Nici un mail pentru %s
 Nu exist� mail nou pentru %s
 Nu exist� fi�ier anterior Nu exist� username-ul Nici o valoare setat� pentru "%s" Nu este destul� memorie Parol� veche: �n primul ecran(screenful) cu mesaje
 �n ultimul ecran(screenful) cu mesaje
 Doar proprietarul(owner) fi�ierului poate folosi --username Opera�iune e�uat� Opera�iune nepermis� Op�iuni sunt: Alte op�iuni PART TIPAR PROG Parol�: Parolele difer�. V� rug�m re�ncerca�i. V� rug�m r�spunde�i yes sau no:  Ap�sa�i <return> pentru a afi�a con�inutul... �nainte a folosi MH, e necesar s� ave�i un fi�ier �n directorul vostru de login
(%s) numit .mh_profile care con�ine informa�ii despre anumite opera�ii directe
MH.  Singurul element care este necesar este calea(path) care se va folosi
pentru toate opera�iile de dosar(folder) MH.  Calea MH sugerat� pentru voi
este %s...
 RE�NCERC�RI Se cite�te %s
 Pune(refile) ciorna �n DOSARul(FOLDER) dat. �terge dosarul %s �nlocuie�te ciorna cu una nou� creat� recent. Raporta�i bug-urile la %s.
 SCRIPT SECUNDE �IR SUBIECT SWITCH-URI S-a salvat %d mesaj �n %s
 S-au salvat %d mesaje �n %s
 Op�iuni de salvare Trimite mesajul �n fundal. Trimite mesajul. Flagul -watch cauzeaz� monitorizarea procesului de livrare. SWITCH-urile sunt pasate pentru a trimite programul verbatim. Seteaz� l��imea output-ului Ne pare r�u TICKET TOTAL Termin� sesiunea. P�streaz� ciorna, �n cazul �n care nu este dat flag-ul -delete. Termin� sesiunea. P�streaz� ciorna. Acesta este %s

 Prea multe argumente pentru comanda de scrolling URL USER NUME_UTILIZATOR Comand� necunoscut�: %s Comand� necunoscut�: %s
 Escape necunoscut %s Schem� de autentificare nesuportat� Folosire: Folose�te "%s"? Folosi�i "." pentru a termina scrisoarea. Folosi�i "quit" pentru a ie�i. Folosi�i "~." pentru a termina scrisoarea. Folose�te acest draft. Folose�te aceast� ciorn� Nu este furnizat username-ul Nu este furnizata parola user-ului Avertismet: .biffrc-ul vostru are permisiuni gre�ite Care este calea �ntreag�?  Care este calea?  �i acum? Ave�i deja un profil MH; folosi�i un editor pentru a-l modifica Nu pute�i specifica mai mult de o op�iune `-aldp' [FI�IER] [mailbox...] `%s' deja inclus la primul(top) nivel `%s' deja inclus aici ac�iunea `%s' nu a fost cerut� nu se poate crea iterator: %s nu se poate trimite mesajul: %s comparatorul %s este incompatibil cu :count �n apelarea c�tre `%s' comparatorul `%s' este incompatibil cu tipul de potrivire `%s' �n apelarea c�tre `%s' livrare �n %s else f�r� if corespunz�tor endif f�r� if corespunz�tor extra argumente format: �mp�r�ire la zero if necesit� un argument: s | r | t �n %4lu dosar �n %4lu dosare potrivire rela�ional� `%s' invalid� �n apelarea c�tre `%s' nume etichet�(tag) invalid `%s' pentru `%s' marcare ca �ters tipul de potrivire este specificat de dou� ori �n apelarea c�tre `%s' switch-uri specifice mesajelor: lipsesc ghilimele de �nchidere �n declara�ie preprocesor nu exist� mesaj alternativ de afi�at nu exist� fi�ier ciorn� de afi�at nici o linie rezultat�(out)
 nici un mesaj �n %s nici un mesaj �n intervalul %s nnici un mesaj urm�tor nici un mesaj anterior nu este destul� memorie doar un singur mesaj odat�! oops? sintax� preprocesor privat eroare de interval(range) incluziune recursiv� ac�iune cerut� comparator cerut test cerut al doilea argument nu poate fi convertit �ntr-un num�r al doilea argument trebuie s� fie o list� de un element sursa pentru %s %s nu este disponibil� subrulare(underflow) a stivei(stack) se stocheaz� mesajul %s partea %s ca fi�ier %s
 testul `%s' nu a fost cerut c�tre %s prea pu�ine argumente �n apelarea c�tre `%s' prea multe argumente pera multe argumente �n apelarea c�tre `%s' prea multe procese children (%lu) nu se poate deschide fi�ierul temporar: %s unalias necesit� cel pu�in un argument func�ie nedefinit� ac�iune necunoscut�: %s tip de mesaj necunoscut test necunoscut: %s | Tip=%s
 